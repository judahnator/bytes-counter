<?php

namespace judahnator;


use judahnator\Exceptions\PrecisionException;

/**
 * Class Bytes
 * @package judahnator\BytesCounter
 * @property string pretty
 * @property int raw
 */
class BytesCounter
{

    private $Bytes;

    private static $SIZE_INDEX = [
        'k' => 3,
        'M' => 6,
        'G' => 9,
        'T' => 12,
        'P' => 15,
        'E' => 18,
        'Z' => 21,
        'Y' => 24
    ];

    /**
     * Bytes constructor.
     * @param int|string $Bytes
     * @throws PrecisionException
     */
    public function __construct($Bytes = 0)
    {

        // If the input is an integer, simply set the bytes to the value and return
        if (is_int($Bytes)) {
            $this->Bytes = $Bytes;
            return;
        }

        // Otherwise, check to see if we have input "pretty" formatted
        if (preg_match("/(\d+\.?\d*)(k|M|G|T|P|E|Z|Y)?B/", $Bytes, $ParsedInput)) {

            if (array_key_exists(2,$ParsedInput)) {

                // If the input has a factor, eg k, M, G, etc...
                // Then get the number of decimal places we can expect
                $factor = static::$SIZE_INDEX[$ParsedInput[2]];

            }else {

                // If no factor has been provided...

                if ((float)$ParsedInput[1] != (int)$ParsedInput[1]) { // Only used because "is_int('1') === false"

                    // Throw an exception because we don't care for fractions of bytes
                    throw new PrecisionException("Fractions of bytes are not allowed");

                }else {

                    // There are no decimal places
                    $factor = 0;

                }
            }

            // If the number of decimal places provided is greater than the number of decimal places
            // allowed by the factor, we will run into the same issue above with fractions of bytes
            if (strlen(substr(strrchr($ParsedInput[1],"."),1)) > $factor)
                throw new PrecisionException("Too many decimal places supplied for the given byte value");

            // Calculate the number of bytes based on a base 1024 storage number
            $this->Bytes = ceil($ParsedInput[1] * pow(1024,$factor / 3));

            // Bail before hitting the exception below
            return;

        }

        // Return an exception
        throw new \InvalidArgumentException("I don't know how to parse '".$Bytes."'");

    }

    /**
     * @param $name
     * @return int|null
     */
    public function __get($name)
    {
        switch($name) {
            case "raw":
                // Some numbers are too large to be cast to an integer, check if an exponent is used
                return strpos($this->Bytes, 'E') === false ? (int)$this->Bytes : $this->Bytes;
            case "pretty":
                $factor = floor((strlen($this->Bytes) - 1) / 3);
                if ($factor > 0) {
                    $human =  sprintf("%.2f", $this->Bytes / pow(1024, $factor));
                }else {
                    $human = $this->Bytes;
                }
                $unit = @array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB')[$factor];
                return $human . $unit;

        }
        return null;
    }

    /**
     * @param int|BytesCounter $Bytes
     * @return $this
     */
    public function add($Bytes = 1): BytesCounter
    {

        if (is_int($Bytes)) {

            // If an integer is passed in, add it to the total
            $this->Bytes += $Bytes;

        }elseif (is_object($Bytes) && get_class($Bytes) === get_class($this)) {

            // If an instance of Bytes is passed in, add its raw total to ours
            $this->Bytes += $Bytes->raw;

        }elseif (is_string($Bytes)) {

            // Try constructing, maybe a parsed value has been input
            $this->Bytes += static::make($Bytes)->raw;

        }else {

            // If an unknown type is provided throw an exception
            throw new \InvalidArgumentException("The input value must be of type integer, BytesCounter, or correctly formatted string");

        }

        // Return $this
        return $this;

    }

    /**
     * @param int $Bytes
     * @return BytesCounter
     * @throws PrecisionException
     */
    public static function make($Bytes = 0): BytesCounter
    {
        return new BytesCounter($Bytes);
    }

    /**
     * @param array $array
     * @return BytesCounter
     * @throws PrecisionException
     */
    public static function sum(array $array): BytesCounter
    {
        $Sum = static::make();
        foreach ($array as $toAdd) {
            $Sum->add($toAdd);
        }
        return $Sum;
    }

}