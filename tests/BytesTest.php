<?php

namespace judahnator\BytesCounter\Tests;

use judahnator\BytesCounter as Bytes;
use PHPUnit\Framework\TestCase;

class BytesTest extends TestCase
{

    /**
     * Determines whether the "pretty" property will work or not
     */
    public function testBasicUsage() {

        // No default value
        $Bytes = new Bytes();

        $this->assertEquals(0,$Bytes->raw);
        $this->assertEquals("0B",$Bytes->pretty);

        // Default value of 1k
        $Bytes = new Bytes(1024);
        $this->assertEquals(1024,$Bytes->raw);
        $this->assertEquals('1.00kB',$Bytes->pretty);

    }

    /**
     * Tests if fluent expression, or chaining functions together, will work.
     */
    public function testFluentUsage() {

        // No default value
        $this->assertEquals(0,Bytes::make()->raw);

        // Default value of 1k
        $this->assertEquals(1024,Bytes::make(1024)->raw);

    }

    /**
     * Tests whether the "add" function works
     */
    public function testAdditionFunctionality() {

        // No default value
        $this->assertEquals(3,Bytes::make()->add()->add()->add()->raw);

        // Test adding integers
        $this->assertEquals(1024,Bytes::make(512)->add(512)->raw);

        // Test adding from Bytes class
        $A = Bytes::make(512);
        $B = Bytes::make(512);
        $this->assertEquals(1024,$A->add($B)->raw);

        // Test adding parsed strings
        $this->assertEquals("2.00MB",Bytes::make("1MB")->add("1MB")->pretty);

    }

    /**
     * Tests the "sum" function
     */
    public function testSumFunctionality() {

        // The array to sum up
        $testData = [
            1,
            2,
            3,
            Bytes::make(4),
            Bytes::make(5)->add(6),
            "19B"
        ];

        $this->assertEquals(40,Bytes::sum($testData)->raw);

    }

    /**
     * Tests the parsing of formatted strings
     */
    public function testStringInputs() {

        // test numeric conversion
        $this->assertEquals(1,Bytes::make("1B")->raw);
        $this->assertEquals(1024,Bytes::make("1kB")->raw);
        $this->assertEquals(1024**2,Bytes::make("1MB")->raw);
        $this->assertEquals(1024**3,Bytes::make("1GB")->raw);
        $this->assertEquals(1024**4,Bytes::make("1TB")->raw);
        $this->assertEquals(1024**5,Bytes::make("1PB")->raw);
        $this->assertEquals(1024**6,Bytes::make("1EB")->raw);
        $this->assertEquals(1024**7,Bytes::make("1ZB")->raw);
        $this->assertEquals(1024**8,Bytes::make("1YB")->raw);

        // make sure the numbers going in is what comes back out
        $this->assertEquals("1B",Bytes::make("1B")->pretty);
        $this->assertEquals("1.00kB",Bytes::make("1.00kB")->pretty);
        $this->assertEquals("1.00MB",Bytes::make("1.00MB")->pretty);
        $this->assertEquals("1.00GB",Bytes::make("1.00GB")->pretty);
        $this->assertEquals("1.00TB",Bytes::make("1.00TB")->pretty);
        // Will not go higher without bcmath extension

    }

    /**
     * Make sure that the correct precision is used for sub-1k bytes
     */
    public function testInvalidDecimalsOnPretty()
    {
        $this->assertEquals("0B", Bytes::make()->pretty);
        $this->assertEquals("42B", Bytes::make(42)->pretty);
        $this->assertEquals("1.23kB",Bytes::make("1.23kB")->pretty);
    }

    /**
     * Make sure that the correct precision is used for raw byte calculations
     */
    public function testInvalidDecimalsOnRaw()
    {
        $this->assertTrue(is_int(Bytes::make('1kB')->raw), 'Unexpected raw return type');
    }

}
